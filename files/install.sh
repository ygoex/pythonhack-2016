bash /home/ubuntu/Anaconda2-4.2.0-Linux-x86_64.sh -b -p $HOME/anaconda2
echo 'export PATH="$HOME/anaconda2/bin:$PATH"' >> /home/ubuntu/.bashrc
export PATH="/home/ubuntu/anaconda2/bin:$PATH"
conda install -y  numpy scipy scikit-learn
pip install deap update_checker tqdm
pip install xgboost
pip install tpot